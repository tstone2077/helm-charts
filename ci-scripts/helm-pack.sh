CI_PROJECT_ID=${CI_PROJECT_ID:-47672373}
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
helm repo add gitlab https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/helm/stable
helm repo update gitlab
echo "*********************"

for chart in $SCRIPT_DIR/../charts/library/common $(find $SCRIPT_DIR/../charts/apps/ -maxdepth 1 -type d)
do
    if test -f $chart/Chart.yaml && grep -q $CI_PROJECT_ID $chart/Chart.yaml
    then
        version=$(yq '.version' $chart/Chart.yaml)
        chart_name=$(yq '.name' $chart/Chart.yaml)
        last_version=$(helm search repo gitlab/$chart_name -o yaml | yq '.[0].version')
        if test $version != $last_version
        then
            echo ------------------
            echo "$chart_name (from $last_version to $version)"
            helm dependency update $chart > /dev/null
            helm package $chart
            CHART_FILE=${chart_name}-$version.tgz
            if ! test -z $CI_PIPELINE_ID
            then
                curl --request POST \
                    --form 'chart=@'$CHART_FILE \
                    --user $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                    https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/helm/api/stable/charts
                if test "$chart" == "charts/library/common"
                then
                    # sleep for 10 seconds so the common chart can be updated in the registry
                    sleep 10
                fi
            fi
        fi
    fi
done
